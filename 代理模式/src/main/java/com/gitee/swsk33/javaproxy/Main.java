package com.gitee.swsk33.javaproxy;

import com.gitee.swsk33.javaproxy.proxy.cglibproxy.CGLibProxyBind;
import com.gitee.swsk33.javaproxy.proxy.jdkproxy.JDKProxyBind;
import com.gitee.swsk33.javaproxy.proxy.staticproxy.TailorStaticAgent;
import com.gitee.swsk33.javaproxy.model.Clothing;
import com.gitee.swsk33.javaproxy.service.ClothesMaking;
import com.gitee.swsk33.javaproxy.service.impl.Tailor;

public class Main {

	/**
	 * 静态代理 有一个客户来定制衣服
	 * 客户定制衬衫，只需要告诉裁缝代理人即可
	 */
	private static void customClothingByStaticProxy() {
		ClothesMaking agent = new TailorStaticAgent();
		Clothing customClothing = agent.makeClothing("衬衫", "XL");
		System.out.println("得到衣服名：" + customClothing.getName() + "；得到衣服尺码：" + customClothing.getSize());
	}

	/**
	 * JDK动态代理 有一个客户来定制衣服
	 */
	private static void customClothingByJDKProxy() {
		// 首先创建JDK代理逻辑实现类的实例，用于将代理对象和委托对象联系起来
		JDKProxyBind jdkProxyBind = new JDKProxyBind();
		// 新建裁缝实例，绑定关系
		ClothesMaking tailor = new Tailor();
		ClothesMaking agent = (ClothesMaking) jdkProxyBind.bind(tailor);
		// 这个时候agent对象已经是一个代理对象了，它会进入代理的逻辑方法invoke中去
		Clothing customClothing = agent.makeClothing("衬衫", "XL");
		System.out.println("得到衣服名：" + customClothing.getName() + "；得到衣服尺码：" + customClothing.getSize());
	}

	/**
	 * CGLib动态代理 有一个客户来定制衣服
	 */
	private static void customClothingByCGLibProxy() {
		// 首先创建CGLib代理逻辑类实例，把代理对象和委托对象联系起来
		CGLibProxyBind cgLibProxyBind = new CGLibProxyBind();
		// 生成裁缝的代理对象
		Tailor agent = (Tailor) cgLibProxyBind.getProxy(Tailor.class);
		// 这个时候agent已经是一个代理对象了，尽管它是Tailor类
		Clothing customClothing = agent.makeClothing("衬衫", "XL");
		System.out.println("得到衣服名：" + customClothing.getName() + "；得到衣服尺码：" + customClothing.getSize());
	}

	public static void main(String[] args) {
		// 静态代理方法
		customClothingByStaticProxy();
		System.out.println();
		// JDK动态代理方法
		customClothingByJDKProxy();
		System.out.println();
		// CGLib动态代理方法
		customClothingByCGLibProxy();
		System.out.println();
	}

}