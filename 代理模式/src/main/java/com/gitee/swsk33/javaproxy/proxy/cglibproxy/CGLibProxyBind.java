package com.gitee.swsk33.javaproxy.proxy.cglibproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * CGLib动态代理逻辑实现
 */
public class CGLibProxyBind implements MethodInterceptor {

	/**
	 * 生成CGLib代理对象
	 *
	 * @param cls 真实对象的类
	 * @return 代理对象
	 */
	public Object getProxy(Class cls) {
		// CGLib增强类型对象
		Enhancer enhancer = new Enhancer();
		// 设定委托类为父类
		enhancer.setSuperclass(cls);
		// 设定代理逻辑类为本类
		enhancer.setCallback(this);
		// 生成并返回代理对象
		return enhancer.create();
	}

	/**
	 * 代理逻辑方法
	 *
	 * @param proxy       代理对象
	 * @param method      方法
	 * @param args        方法参数
	 * @param methodProxy 方法代理
	 * @return 代理逻辑返回
	 * @throws Throwable 异常
	 */
	@Override
	public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
		System.out.println("[进入CGLib动态代理逻辑]");
		System.out.println("裁缝代理人已接到需求！");
		// 调用裁缝（委托类）的方法
		Object result = methodProxy.invokeSuper(proxy, args);
		System.out.println("衣服定制完成！");
		return result;
	}

}