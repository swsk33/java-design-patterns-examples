package com.gitee.swsk33.javaproxy.service;

import com.gitee.swsk33.javaproxy.model.Clothing;

/**
 * 衣服制作接口
 */
public interface ClothesMaking {

	/**
	 * 衣服制作
	 * @param name 衣服名
	 * @param size 衣服大小
	 * @return 衣服实例
	 */
	Clothing makeClothing(String name, String size);

}