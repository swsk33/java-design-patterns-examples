package com.gitee.swsk33.javaproxy.proxy.staticproxy;

import com.gitee.swsk33.javaproxy.model.Clothing;
import com.gitee.swsk33.javaproxy.service.ClothesMaking;
import com.gitee.swsk33.javaproxy.service.impl.Tailor;

/**
 * 裁缝代理人（代理类，静态代理）
 */
public class TailorStaticAgent implements ClothesMaking {

	/**
	 * 代理人的裁缝实例（委托对象）
	 */
	private ClothesMaking tailor = new Tailor();

	/**
	 * 向代理人定制衣服
	 *
	 * @param name   衣服名字
	 * @param size 客户身高
	 * @return 衣服
	 */
	@Override
	public Clothing makeClothing(String name, String size) {
		System.out.println("[进入静态代理逻辑]");
		System.out.println("裁缝代理人已接到需求！");
		return tailor.makeClothing(name, size);
	}

}