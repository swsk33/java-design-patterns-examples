package com.gitee.swsk33.javaproxy.proxy.jdkproxy;

import com.gitee.swsk33.javaproxy.service.ClothesMaking;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK动态代理逻辑实现
 */
public class JDKProxyBind implements InvocationHandler {

	/**
	 * 代理人的裁缝代理实例（委托对象）
	 */
	private ClothesMaking tailor;

	/**
	 * 将委托对象和代理对象建立关系
	 *
	 * @param origin 委托对象
	 * @return 代理对象
	 */
	public Object bind(ClothesMaking origin) {
		this.tailor = origin;
		return Proxy.newProxyInstance(tailor.getClass().getClassLoader(), tailor.getClass().getInterfaces(), this);
	}

	/**
	 * 代理逻辑方法
	 *
	 * @param proxy  代理对象
	 * @param method 当前调度方法
	 * @param args   当前方法参数
	 * @return 代理结果
	 * @throws Throwable 异常
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("[进入JDK动态代理逻辑]");
		System.out.println("裁缝代理人已接到需求！");
		// 调用裁缝（委托类）的方法
		Object result = method.invoke(tailor, args);
		System.out.println("衣服定制完成！");
		return result;
	}

}