package com.gitee.swsk33.javaproxy.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 衣服类
 */
@Getter
@Setter
public class Clothing {

	/**
	 * 衣服名
	 */
	private String name;

	/**
	 * 尺码（S,M,L,XL,XXL）
	 */
	private String size;

}