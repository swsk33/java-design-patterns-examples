package com.gitee.swsk33.javaproxy.service.impl;

import com.gitee.swsk33.javaproxy.model.Clothing;
import com.gitee.swsk33.javaproxy.service.ClothesMaking;

/**
 * 裁缝（委托类）
 */
public class Tailor implements ClothesMaking {

	@Override
	public Clothing makeClothing(String name, String size) {
		Clothing clothing = new Clothing();
		clothing.setName(name);
		clothing.setSize(size);
		return clothing;
	}

}