package com.gitee.swsk33.decorator.wrapper.impl;

import com.gitee.swsk33.decorator.model.Gun;
import com.gitee.swsk33.decorator.wrapper.GunWrapper;

/**
 * 八倍镜部件
 */
public class EightfoldScope extends GunWrapper {

	/**
	 * 构造器依赖注入
	 *
	 * @param gun 要安装部件的枪械
	 */
	public EightfoldScope(Gun gun) {
		super(gun);
		this.setStatus(gun.getStatus() + " + 八倍镜");
		this.setCapability(gun.getCapability() + 5);
	}

}