package com.gitee.swsk33.decorator.wrapper;

import com.gitee.swsk33.decorator.model.Gun;

/**
 * 枪的部件
 */
public abstract class GunWrapper extends Gun {

	/**
	 * 传入要安装部件的枪械
	 */
	private Gun gun;

	/**
	 * 构造器依赖注入
	 *
	 * @param gun 要安装部件的枪械
	 */
	public GunWrapper(Gun gun) {
		this.gun = gun;
	}

}