package com.gitee.swsk33.decorator.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 枪械
 */
@Getter
@Setter
public abstract class Gun {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 当前武器状态
	 */
	private String status;

	/**
	 * 武器性能
	 */
	private int capability;

}