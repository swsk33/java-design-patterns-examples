package com.gitee.swsk33.decorator;

import com.gitee.swsk33.decorator.model.Gun;
import com.gitee.swsk33.decorator.model.impl.Kar98K;
import com.gitee.swsk33.decorator.wrapper.impl.BulletBag;
import com.gitee.swsk33.decorator.wrapper.impl.EightfoldScope;

/**
 * 玩家类
 */
public class Player {

	public static void main(String[] args) {
		// 模拟玩家正在吃鸡游戏
		// 捡到一把98K步枪
		Gun kar98k = new Kar98K();
		System.out.println("当前武器情况：" + kar98k.getStatus() + "，性能：" + kar98k.getCapability());
		// 捡到一个八倍镜并安装至98K上面
		kar98k = new EightfoldScope(kar98k);
		System.out.println("当前武器情况：" + kar98k.getStatus() + "，性能：" + kar98k.getCapability());
		// 捡到一个子弹袋并安装至98K上面
		kar98k = new BulletBag(kar98k);
		System.out.println("当前武器情况：" + kar98k.getStatus() + "，性能：" + kar98k.getCapability());
	}

}