package com.gitee.swsk33.decorator.wrapper.impl;

import com.gitee.swsk33.decorator.model.Gun;
import com.gitee.swsk33.decorator.wrapper.GunWrapper;

/**
 * 子弹袋部件
 */
public class BulletBag extends GunWrapper {

	/**
	 * 构造器依赖注入
	 *
	 * @param gun 要安装部件的枪械
	 */
	public BulletBag(Gun gun) {
		super(gun);
		this.setStatus(gun.getStatus() + " + 子弹袋");
		this.setCapability(gun.getCapability() + 3);
	}

}