package com.gitee.swsk33.decorator.model.impl;

import com.gitee.swsk33.decorator.model.Gun;

/**
 * 98K步枪
 */
public class Kar98K extends Gun {

	/**
	 * 构造函数：初始化属性
	 */
	public Kar98K() {
		setName("Kar98K");
		setStatus("98K步枪");
		setCapability(10);
	}

}