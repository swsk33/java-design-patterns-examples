package com.gitee.swsk33.observerpatterndemo.model.prototype;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象主题，即被观察的对象
 */
@Data
public abstract class Subject {

	/**
	 * 主题名称
	 */
	private String name;

	/**
	 * 主题状态
	 */
	private String state;

	/**
	 * 全部观察这个主题的观察者列表
	 */
	private final List<Observer> observers = new ArrayList<>();

	/**
	 * 添加一个观察者对象到观察者列表
	 *
	 * @param observer 要添加的观察者对象
	 */
	public void register(Observer observer) {
		if (observer != null) {
			observers.add(observer);
		}
	}

	/**
	 * 从观察者列表移除一个观察者
	 *
	 * @param observer 要移除的观察者
	 */
	public void remove(Observer observer) {
		observers.remove(observer);
	}

	/**
	 * 当主题发生变化时，通知全部观察者
	 */
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update(this);
		}
	}

}