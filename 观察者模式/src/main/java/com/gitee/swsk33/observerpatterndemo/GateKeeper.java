package com.gitee.swsk33.observerpatterndemo;

import com.gitee.swsk33.observerpatterndemo.model.Dean;
import com.gitee.swsk33.observerpatterndemo.model.StudentPlayingGame;
import com.gitee.swsk33.observerpatterndemo.model.StudentWatchingTV;
import com.gitee.swsk33.observerpatterndemo.model.Teacher;
import com.gitee.swsk33.observerpatterndemo.model.prototype.Subject;

/**
 * 主类：在门口的学生
 */
public class GateKeeper {

	public static void main(String[] args) {
		// 创建被观察主题对象
		Subject teacher = new Teacher();
		teacher.setName("张老师");
		teacher.setState("在办公室");
		Subject dean = new Dean();
		dean.setName("刘院长");
		dean.setState("在办公室");
		// 创建观察者对象
		StudentWatchingTV tvStudent = new StudentWatchingTV();
		tvStudent.setName("小吴");
		tvStudent.setTvName("甄嬛传");
		StudentPlayingGame gameStudent = new StudentPlayingGame();
		gameStudent.setName("小李");
		gameStudent.setGameName("原神");
		// 将观察者对象注册到每个主题
		teacher.register(tvStudent);
		teacher.register(gameStudent);
		dean.register(tvStudent);
		dean.register(gameStudent);
		// 主题对象改变，通知所有观察者
		teacher.setState("从走廊过来");
		teacher.notifyObservers();
		dean.setState("到研究室门口了");
		dean.notifyObservers();
	}

}