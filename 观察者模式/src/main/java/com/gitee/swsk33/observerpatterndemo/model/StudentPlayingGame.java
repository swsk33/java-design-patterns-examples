package com.gitee.swsk33.observerpatterndemo.model;

import com.gitee.swsk33.observerpatterndemo.model.prototype.Observer;
import com.gitee.swsk33.observerpatterndemo.model.prototype.Subject;
import lombok.Data;

/**
 * 具体观察者：打游戏的学生
 */
@Data
public class StudentPlayingGame extends Observer {

	/**
	 * 正在玩的游戏名称
	 */
	private String gameName;

	@Override
	public void update(Subject subject) {
		// 实现自定义的接受到消息后的逻辑
		System.out.printf("%s，发现%s正在%s，不要玩%s了！\n", getName(), subject.getName(), subject.getState(), gameName);
	}

}