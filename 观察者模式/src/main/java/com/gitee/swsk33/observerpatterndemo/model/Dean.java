package com.gitee.swsk33.observerpatterndemo.model;

import com.gitee.swsk33.observerpatterndemo.model.prototype.Subject;
import lombok.Data;

/**
 * 具体主题（被观察对象）：院长
 */
@Data
public class Dean extends Subject {

}