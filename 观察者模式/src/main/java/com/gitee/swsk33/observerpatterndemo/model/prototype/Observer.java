package com.gitee.swsk33.observerpatterndemo.model.prototype;

import lombok.Data;

/**
 * 抽象观察者，即观察主题变化的类
 */
@Data
public abstract class Observer {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 更新方法，被观测主题发生变化时，调用该方法传入更新状态后的主题，实现通知观察者，具体观察者通过该方法实现具体接受到通知后的逻辑
	 *
	 * @param subject 更新状态后的主题对象
	 */
	public abstract void update(Subject subject);

}