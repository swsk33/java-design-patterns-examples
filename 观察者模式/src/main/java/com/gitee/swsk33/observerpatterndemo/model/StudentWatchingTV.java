package com.gitee.swsk33.observerpatterndemo.model;

import com.gitee.swsk33.observerpatterndemo.model.prototype.Observer;
import com.gitee.swsk33.observerpatterndemo.model.prototype.Subject;
import lombok.Data;

/**
 * 具体观察者：看电视剧的学生
 */
@Data
public class StudentWatchingTV extends Observer {

	/**
	 * 正在看的电视剧名称
	 */
	private String tvName;

	@Override
	public void update(Subject subject) {
		// 实现自定义的接受到消息后的逻辑
		System.out.printf("%s，发现%s正在%s，快关掉%s！\n", getName(), subject.getName(), subject.getState(), tvName);
	}

}