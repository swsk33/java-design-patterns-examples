package com.gitee.swsk33.simplefactoryreflect;

import com.gitee.swsk33.simplefactoryreflect.factory.FruitFactory;
import com.gitee.swsk33.simplefactoryreflect.model.Watermelon;
import com.gitee.swsk33.simplefactoryreflect.model.prototype.Fruit;

/**
 * 水果店类，卖水果
 */
public class Store {

	public static void main(String[] args) {
		// 来了个顾客
		Fruit getFruit = FruitFactory.get(Watermelon.class);
		System.out.println("得到水果类型：" + getFruit.getClass().getSimpleName());
	}

}