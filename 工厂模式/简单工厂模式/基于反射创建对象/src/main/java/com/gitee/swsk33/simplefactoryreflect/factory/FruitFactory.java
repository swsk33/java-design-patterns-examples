package com.gitee.swsk33.simplefactoryreflect.factory;

import com.gitee.swsk33.simplefactoryreflect.model.prototype.Fruit;

/**
 * 水果工厂类
 */
public class FruitFactory {

	/**
	 * 根据口味生产相应的水果，使用反射替换条件判断
	 *
	 * @param fruitType 传入具体需要的水果类型
	 * @return 对应的水果
	 */
	public static Fruit get(Class<? extends Fruit> fruitType) {
		Fruit fruit = null;
		try {
			// 使用传入类型的构造器创建对象
			fruit = fruitType.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			System.out.println("创建水果出错！");
			e.printStackTrace();
		}
		return fruit;
	}

}