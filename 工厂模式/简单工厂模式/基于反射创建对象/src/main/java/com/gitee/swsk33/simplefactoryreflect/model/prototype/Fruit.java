package com.gitee.swsk33.simplefactoryreflect.model.prototype;

import lombok.Data;

/**
 * 水果抽象类
 */
@Data
public abstract class Fruit {

	/**
	 * 水果名
	 */
	private String name;

}