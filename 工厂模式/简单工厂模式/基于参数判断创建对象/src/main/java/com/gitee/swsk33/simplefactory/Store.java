package com.gitee.swsk33.simplefactory;

import com.gitee.swsk33.simplefactory.factory.FruitFactory;
import com.gitee.swsk33.simplefactory.model.prototype.Fruit;
import com.gitee.swsk33.simplefactory.param.Flavor;

/**
 * 水果店类，卖水果
 */
public class Store {

	public static void main(String[] args) {
		// 来了个顾客
		Fruit getFruit = FruitFactory.get(Flavor.SWEET_ACID);
		System.out.println("得到水果：" + getFruit.getName());
	}

}