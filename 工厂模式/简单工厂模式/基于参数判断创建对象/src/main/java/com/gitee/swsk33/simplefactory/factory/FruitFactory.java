package com.gitee.swsk33.simplefactory.factory;

import com.gitee.swsk33.simplefactory.model.Apple;
import com.gitee.swsk33.simplefactory.model.Orange;
import com.gitee.swsk33.simplefactory.model.Watermelon;
import com.gitee.swsk33.simplefactory.model.prototype.Fruit;
import com.gitee.swsk33.simplefactory.param.Flavor;

/**
 * 水果工厂类
 */
public class FruitFactory {

	/**
	 * 根据口味生产相应的水果
	 *
	 * @param flavor 传入口味枚举参数
	 * @return 对应的水果
	 */
	public static Fruit get(Flavor flavor) {
		Fruit fruit = null;
		switch (flavor) {
			case SWEET -> {
				Watermelon watermelon = new Watermelon();
				watermelon.setName("西瓜");
				watermelon.setCooked(true);
				fruit = watermelon;
			}
			case SWEET_ACID -> {
				Apple apple = new Apple();
				apple.setName("苹果");
				apple.setSweetness(100);
				fruit = apple;
			}
			case ACID -> {
				Orange orange = new Orange();
				orange.setName("橙子");
				orange.setAcidity(12);
				fruit = orange;
			}
		}
		return fruit;
	}

}