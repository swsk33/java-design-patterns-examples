package com.gitee.swsk33.simplefactory.model;

import com.gitee.swsk33.simplefactory.model.prototype.Fruit;
import lombok.Data;

/**
 * 西瓜类
 */
@Data
public class Watermelon extends Fruit {

	/**
	 * 是否保熟
	 */
	private boolean cooked;

}