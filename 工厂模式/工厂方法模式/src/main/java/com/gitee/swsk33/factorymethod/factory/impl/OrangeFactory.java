package com.gitee.swsk33.factorymethod.factory.impl;

import com.gitee.swsk33.factorymethod.factory.FruitFactory;
import com.gitee.swsk33.factorymethod.model.Orange;
import com.gitee.swsk33.factorymethod.model.prototype.Fruit;

/**
 * 生产橙子的具体工厂
 */
public class OrangeFactory implements FruitFactory {

	@Override
	public Fruit get() {
		Orange orange = new Orange();
		orange.setName("橙子");
		orange.setAcidity(5);
		return orange;
	}

}