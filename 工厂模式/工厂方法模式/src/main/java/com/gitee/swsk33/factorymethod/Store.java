package com.gitee.swsk33.factorymethod;

import com.gitee.swsk33.factorymethod.factory.FruitFactory;
import com.gitee.swsk33.factorymethod.factory.impl.AppleFactory;
import com.gitee.swsk33.factorymethod.model.prototype.Fruit;

/**
 * 水果店类，卖水果
 */
public class Store {

	public static void main(String[] args) {
		// 需要对应的水果时，实例化一个具体工厂即可
		// 比如说现在要苹果
		FruitFactory factory = new AppleFactory();
		Fruit fruit = factory.get();
		System.out.println("得到水果：" + fruit.getName());
	}

}