package com.gitee.swsk33.factorymethod.factory.impl;

import com.gitee.swsk33.factorymethod.factory.FruitFactory;
import com.gitee.swsk33.factorymethod.model.Apple;
import com.gitee.swsk33.factorymethod.model.prototype.Fruit;

/**
 * 生产苹果的具体工厂
 */
public class AppleFactory implements FruitFactory {

	@Override
	public Fruit get() {
		Apple apple = new Apple();
		apple.setName("苹果");
		apple.setSweetness(12);
		return apple;
	}

}