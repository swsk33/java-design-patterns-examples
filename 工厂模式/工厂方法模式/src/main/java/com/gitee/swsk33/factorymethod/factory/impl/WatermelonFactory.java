package com.gitee.swsk33.factorymethod.factory.impl;

import com.gitee.swsk33.factorymethod.factory.FruitFactory;
import com.gitee.swsk33.factorymethod.model.Watermelon;
import com.gitee.swsk33.factorymethod.model.prototype.Fruit;

/**
 * 生产西瓜的具体工厂
 */
public class WatermelonFactory implements FruitFactory {

	@Override
	public Fruit get() {
		Watermelon watermelon = new Watermelon();
		watermelon.setName("西瓜");
		watermelon.setCooked(true);
		return watermelon;
	}

}