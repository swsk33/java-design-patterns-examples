package com.gitee.swsk33.factorymethod.model;

import com.gitee.swsk33.factorymethod.model.prototype.Fruit;
import lombok.Data;

/**
 * 橙子类
 */
@Data
public class Orange extends Fruit {

	/**
	 * 酸度
	 */
	private int acidity;

}