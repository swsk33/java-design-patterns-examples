package com.gitee.swsk33.factorymethod.factory;

import com.gitee.swsk33.factorymethod.model.prototype.Fruit;

/**
 * 水果抽象工厂类
 */
public interface FruitFactory {

	/**
	 * 根据口味生产相应的水果
	 *
	 * @return 对应的水果
	 */
	Fruit get();

}