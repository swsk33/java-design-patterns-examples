package com.gitee.swsk33.abstractfactory.factory.builder;

import com.gitee.swsk33.abstractfactory.factory.FruitFoodFactory;
import com.gitee.swsk33.abstractfactory.factory.impl.DrinkFactory;
import com.gitee.swsk33.abstractfactory.factory.impl.FruitFactory;
import com.gitee.swsk33.abstractfactory.param.Type;

import java.util.HashMap;
import java.util.Map;

/**
 * 工厂建造器
 */
public class FruitFoodFactoryBuilder {

	/**
	 * 存放不同类型对应的具体工厂的实例
	 */
	private static final Map<Type, FruitFoodFactory> FACTORY_MAP = new HashMap<>();

	static {
		// 初始化工厂列表
		FACTORY_MAP.put(Type.FRUIT, new FruitFactory());
		FACTORY_MAP.put(Type.DRINK, new DrinkFactory());
	}

	/**
	 * 根据客户的需求类型建造出对应的工厂
	 *
	 * @param type 抽象类型枚举参数
	 * @return 对应的工厂实例
	 */
	public static FruitFoodFactory getFactory(Type type) {
		return FACTORY_MAP.get(type);
	}

}