package com.gitee.swsk33.abstractfactory.factory.impl;

import com.gitee.swsk33.abstractfactory.factory.FruitFoodFactory;
import com.gitee.swsk33.abstractfactory.model.Apple;
import com.gitee.swsk33.abstractfactory.model.Orange;
import com.gitee.swsk33.abstractfactory.model.Watermelon;
import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import com.gitee.swsk33.abstractfactory.model.prototype.Fruit;
import com.gitee.swsk33.abstractfactory.param.Flavor;

/**
 * 水果工厂类
 */
public class FruitFactory implements FruitFoodFactory {

	@Override
	public Fruit getFruit(Flavor flavor) {
		Fruit fruit = null;
		switch (flavor) {
			case SWEET -> {
				Watermelon watermelon = new Watermelon();
				watermelon.setName("西瓜");
				watermelon.setCooked(true);
				fruit = watermelon;
			}
			case SWEET_ACID -> {
				Apple apple = new Apple();
				apple.setName("苹果");
				apple.setSweetness(100);
				fruit = apple;
			}
			case ACID -> {
				Orange orange = new Orange();
				orange.setName("橙子");
				orange.setAcidity(12);
				fruit = orange;
			}
		}
		return fruit;
	}

	@Override
	public Drink getDrink(Flavor flavor) {
		return null;
	}

}