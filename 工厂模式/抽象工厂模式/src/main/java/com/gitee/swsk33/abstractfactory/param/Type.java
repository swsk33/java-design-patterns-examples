package com.gitee.swsk33.abstractfactory.param;

/**
 * 客户需求商品类型
 */
public enum Type {
	/**
	 * 水果
	 */
	FRUIT,
	/**
	 * 饮料
	 */
	DRINK
}