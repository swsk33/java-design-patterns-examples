package com.gitee.swsk33.abstractfactory;

import com.gitee.swsk33.abstractfactory.factory.FruitFoodFactory;
import com.gitee.swsk33.abstractfactory.factory.builder.FruitFoodFactoryBuilder;
import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import com.gitee.swsk33.abstractfactory.param.Flavor;
import com.gitee.swsk33.abstractfactory.param.Type;

/**
 * 商店类
 */
public class Store {

	public static void main(String[] args) {
		// 需要饮料、口味酸
		// 首先获取对应的工厂
		FruitFoodFactory factory = FruitFoodFactoryBuilder.getFactory(Type.DRINK);
		// 工厂生产物品
		Drink drink = factory.getDrink(Flavor.ACID);
		System.out.println("顾客购买：" + drink.getName());
	}

}