package com.gitee.swsk33.abstractfactory.param;

/**
 * 客户口味参数
 */
public enum Flavor {
	/**
	 * 甜
	 */
	SWEET,
	/**
	 * 酸甜
	 */
	SWEET_ACID,
	/**
	 * 酸
	 */
	ACID
}