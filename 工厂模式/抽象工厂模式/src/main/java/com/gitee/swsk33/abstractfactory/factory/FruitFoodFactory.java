package com.gitee.swsk33.abstractfactory.factory;

import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import com.gitee.swsk33.abstractfactory.model.prototype.Fruit;
import com.gitee.swsk33.abstractfactory.param.Flavor;

/**
 * 水果食品工厂抽象
 */
public interface FruitFoodFactory {

	/**
	 * 获取水果
	 *
	 * @param flavor 口味枚举参数
	 * @return 根据顾客口味获得对应水果
	 */
	Fruit getFruit(Flavor flavor);

	/**
	 * 获取饮料
	 *
	 * @param flavor 口味枚举参数
	 * @return 根据顾客口味获得对应水果饮料
	 */
	Drink getDrink(Flavor flavor);

}