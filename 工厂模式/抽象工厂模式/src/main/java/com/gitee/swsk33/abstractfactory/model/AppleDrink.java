package com.gitee.swsk33.abstractfactory.model;

import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import lombok.Data;

/**
 * 苹果汁
 */
@Data
public class AppleDrink extends Drink {

	/**
	 * 甜度
	 */
	private int sweetness;

}