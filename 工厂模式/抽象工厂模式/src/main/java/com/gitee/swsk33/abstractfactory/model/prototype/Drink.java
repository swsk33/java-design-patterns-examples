package com.gitee.swsk33.abstractfactory.model.prototype;

import lombok.Data;

/**
 * 饮品抽象类
 */
@Data
public abstract class Drink {

	/**
	 * 饮品名
	 */
	private String name;

}