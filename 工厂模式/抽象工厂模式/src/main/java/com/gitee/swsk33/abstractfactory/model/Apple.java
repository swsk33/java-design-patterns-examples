package com.gitee.swsk33.abstractfactory.model;

import com.gitee.swsk33.abstractfactory.model.prototype.Fruit;
import lombok.Data;

/**
 * 苹果类
 */
@Data
public class Apple extends Fruit {

	/**
	 * 甜度
	 */
	private int sweetness;

}