package com.gitee.swsk33.abstractfactory.model;

import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import lombok.Data;

/**
 * 西瓜汁
 */
@Data
public class WatermelonDrink extends Drink {

	/**
	 * 是否保熟
	 */
	private boolean cooked;

}