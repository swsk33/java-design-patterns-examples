package com.gitee.swsk33.abstractfactory.model;

import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import lombok.Data;

/**
 * 橙汁
 */
@Data
public class OrangeDrink extends Drink {

	/**
	 * 酸度
	 */
	private int acidity;

}