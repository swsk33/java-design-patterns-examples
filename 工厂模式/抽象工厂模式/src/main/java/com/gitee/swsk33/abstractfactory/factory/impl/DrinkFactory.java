package com.gitee.swsk33.abstractfactory.factory.impl;

import com.gitee.swsk33.abstractfactory.factory.FruitFoodFactory;
import com.gitee.swsk33.abstractfactory.model.AppleDrink;
import com.gitee.swsk33.abstractfactory.model.OrangeDrink;
import com.gitee.swsk33.abstractfactory.model.WatermelonDrink;
import com.gitee.swsk33.abstractfactory.model.prototype.Drink;
import com.gitee.swsk33.abstractfactory.model.prototype.Fruit;
import com.gitee.swsk33.abstractfactory.param.Flavor;

public class DrinkFactory implements FruitFoodFactory {

	@Override
	public Fruit getFruit(Flavor flavor) {
		return null;
	}

	@Override
	public Drink getDrink(Flavor flavor) {
		Drink drink = null;
		switch (flavor) {
			case SWEET -> {
				WatermelonDrink watermelonDrink = new WatermelonDrink();
				watermelonDrink.setName("西瓜汁");
				drink = watermelonDrink;
			}
			case SWEET_ACID -> {
				AppleDrink appleDrink = new AppleDrink();
				appleDrink.setName("苹果汁");
				drink = appleDrink;
			}
			case ACID -> {
				OrangeDrink orangeDrink = new OrangeDrink();
				orangeDrink.setName("橙汁");
				drink = orangeDrink;
			}
		}
		return drink;
	}

}