package com.gitee.swsk33.singleinstance.lazy;

import lombok.Getter;
import lombok.Setter;

/**
 * 懒汉式内部类法单例模式
 */
@Getter
@Setter
public class Master {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 私有化构造器
	 */
	private Master() {
		if (InnerMaster.INSTANCE != null) {
			throw new RuntimeException("不允许创建多个实例！");
		}
	}

	/**
	 * 获取老板唯一单例，final使得该方法不允许被重写或者重载
	 *
	 * @return 老板唯一单例
	 */
	public static final Master getInstance() {
		// 返回结果之前，会先加载内部类
		return InnerMaster.INSTANCE;
	}

	/**
	 * 禁止反序列化
	 */
	private Object readResolve() {
		return InnerMaster.INSTANCE;
	}

	/**
	 * 老板类的内部类，没有用到它就不会加载
	 */
	private static class InnerMaster {
		private static final Master INSTANCE = new Master();
	}

}