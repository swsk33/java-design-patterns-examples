package com.gitee.swsk33.singleinstance;

import com.gitee.swsk33.singleinstance.enumerate.Master;

public class Main {

	public static void main(String[] args) {
		Master master = Master.getInstance();
		master.setName("老板");
		System.out.println(master.getName());
	}

}