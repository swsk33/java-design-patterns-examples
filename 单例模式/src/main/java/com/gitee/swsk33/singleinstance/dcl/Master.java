package com.gitee.swsk33.singleinstance.dcl;

import lombok.Data;

/**
 * 懒汉式双检锁单例模式
 */
@Data
public class Master {

	/**
	 * 老板的唯一单例
	 */
	private static volatile Master instance;

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 私有化构造器
	 */
	private Master() {

	}

	/**
	 * 获取老板唯一单例，使用双检锁延迟初始化
	 *
	 * @return 老板唯一单例
	 */
	public static Master getInstance() {
		// 第一次判断单例是否为空
		if (instance == null) {
			// 锁住整个类
			synchronized (Master.class) {
				// 第二次判断单例是否为空，确保只有一个线程创建对象
				if (instance == null) {
					// 初始化单例
					instance = new Master();
				}
			}
		}
		// 返回单例
		return instance;
	}

}