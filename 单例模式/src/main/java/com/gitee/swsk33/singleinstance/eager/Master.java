package com.gitee.swsk33.singleinstance.eager;

import lombok.Getter;
import lombok.Setter;

/**
 * 饿汉式单例模式
 */
@Getter
@Setter
public class Master {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 唯一单例
	 */
	private static Master instance = new Master();

	/**
	 * 私有化构造器
	 */
	private Master() {
	}

	/**
	 * 获取老板唯一单例
	 *
	 * @return 老板唯一单例
	 */
	public static Master getInstance() {
		return instance;
	}

}