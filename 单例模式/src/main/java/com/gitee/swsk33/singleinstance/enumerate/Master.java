package com.gitee.swsk33.singleinstance.enumerate;

import lombok.Getter;
import lombok.Setter;

/**
 * 枚举式单例模式
 */
@Getter
public enum Master {

	/**
	 * 老板类唯一单例
	 */
	INSTANCE;

	/**
	 * 名字
	 */
	@Setter
	private String name;

	/**
	 * 获取老板类唯一实例
	 *
	 * @return 老板类唯一实例
	 */
	public static Master getInstance() {
		return INSTANCE;
	}

}