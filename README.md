# 常用Java设计模式示例

## 介绍

常用的Java设计模式示例程序。

工程中的`dio`文件是Draw.io的类图文件，在VSCode中安装`Draw.io Integration`这个插件即可打开编辑，或者[下载](https://www.drawio.com/)Draw.io软件。