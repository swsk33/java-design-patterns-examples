package com.gitee.swsk33.chainofresponsibility.handler.impl;

import com.gitee.swsk33.chainofresponsibility.handler.SmokeValidator;
import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;

/**
 * 检验学生是否存在把烟夹在耳朵上的动作行为（具体校验者）
 */
public class TakingValidator extends SmokeValidator {

	@Override
	public boolean validateBehavior(StudentBehavior behavior) {
		// 检查学生带走薯条时的行为
		if (behavior.getTakingBehavior().contains("夹耳朵上")) {
			System.out.println("确认学生把烟夹在耳朵上的行为，校验失败！");
			return false;
		}
		// 否则本次校验成功，交给下一处理器
		return passToNext(behavior);
	}

}