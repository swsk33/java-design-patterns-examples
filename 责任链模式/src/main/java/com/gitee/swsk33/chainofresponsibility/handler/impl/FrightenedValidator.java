package com.gitee.swsk33.chainofresponsibility.handler.impl;

import com.gitee.swsk33.chainofresponsibility.handler.SmokeValidator;
import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;

/**
 * 检验学生受到惊吓时是否会心虚（具体校验者）
 */
public class FrightenedValidator extends SmokeValidator {

	@Override
	public boolean validateBehavior(StudentBehavior behavior) {
		// 检查学生受到惊吓时的行为
		if (behavior.getFrightenedBehavior().contains("踩")) {
			System.out.println("确认学生会把烟放在地上踩灭，校验失败！");
			return false;
		}
		// 否则本次校验成功，交给下一处理器
		return passToNext(behavior);
	}

}