package com.gitee.swsk33.chainofresponsibility.handler;

import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;
import lombok.Data;

/**
 * 老师检查学生抽烟行为的校验类（抽象处理者）
 */
@Data
public abstract class SmokeValidator {

	/**
	 * 下一个处理者
	 */
	private SmokeValidator next;

	/**
	 * 检验行为对象，如果行为符合则交给下一个校验器进行校验，否则直接返回失败
	 *
	 * @param behavior 被校验的行为对象
	 * @return 校验结果
	 */
	public abstract boolean validateBehavior(StudentBehavior behavior);

	/**
	 * 检查下一个处理者是否存在，如果存在则将请求对象交给下一个处理者，否则说明当前是最后一个处理者，全部步骤处理完成
	 *
	 * @param behavior 被校验行为对象
	 * @return 校验结果
	 */
	protected boolean passToNext(StudentBehavior behavior) {
		if (next == null) {
			System.out.println("该学生无抽烟行为，校验通过！");
			return true;
		}
		return next.validateBehavior(behavior);
	}

}