package com.gitee.swsk33.chainofresponsibility.handler.impl;

import com.gitee.swsk33.chainofresponsibility.handler.SmokeValidator;
import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;

/**
 * 检验学生是否存在弹烟灰动作行为（具体校验者）
 */
public class FlickAshesValidator extends SmokeValidator {

	@Override
	public boolean validateBehavior(StudentBehavior behavior) {
		// 检查学生蘸番茄酱时的行为
		if (behavior.getHandleExtraKetchup().contains("弹")) {
			System.out.println("确认学生有弹烟灰的行为，校验失败！");
			return false;
		}
		// 否则本次校验成功，交给下一处理器
		return passToNext(behavior);
	}

}