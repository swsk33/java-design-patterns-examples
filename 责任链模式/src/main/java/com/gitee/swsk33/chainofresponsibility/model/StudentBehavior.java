package com.gitee.swsk33.chainofresponsibility.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生行为（被校验对象）
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentBehavior {

	/**
	 * 处理多余的番茄酱的方式
	 */
	private String handleExtraKetchup;

	/**
	 * 带走薯条的方式
	 */
	private String takingBehavior;

	/**
	 * 受到惊吓的方式
	 */
	private String frightenedBehavior;

}