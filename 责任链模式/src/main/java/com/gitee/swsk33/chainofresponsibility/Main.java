package com.gitee.swsk33.chainofresponsibility;

import com.gitee.swsk33.chainofresponsibility.handler.context.SmokeValidatorContext;
import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;

public class Main {

	// 调用端
	public static void main(String[] args) {
		// 老师开始检查几个学生是否抽烟
		// 传来了几个需要校验的对象
		StudentBehavior behaviorOne = new StudentBehavior("弹掉", "夹耳朵上", "踩碎");
		StudentBehavior behaviorTwo = new StudentBehavior("刮掉", "夹耳朵上", "踩碎");
		StudentBehavior behaviorThree = new StudentBehavior("刮掉", "用手拿着", "踩碎");
		StudentBehavior behaviorFour = new StudentBehavior("刮掉", "用手拿着", "无动于衷");
		// 老师使用责任链模式思想校验学生行为
		System.out.println("第1个学生是否不抽烟？" + SmokeValidatorContext.validateStudentBehavior(behaviorOne));
		System.out.println("第2个学生是否不抽烟？" + SmokeValidatorContext.validateStudentBehavior(behaviorTwo));
		System.out.println("第3个学生是否不抽烟？" + SmokeValidatorContext.validateStudentBehavior(behaviorThree));
		System.out.println("第4个学生是否不抽烟？" + SmokeValidatorContext.validateStudentBehavior(behaviorFour));
	}

}