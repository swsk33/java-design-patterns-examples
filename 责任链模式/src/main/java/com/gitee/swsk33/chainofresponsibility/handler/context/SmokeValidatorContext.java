package com.gitee.swsk33.chainofresponsibility.handler.context;

import com.gitee.swsk33.chainofresponsibility.handler.SmokeValidator;
import com.gitee.swsk33.chainofresponsibility.handler.impl.FlickAshesValidator;
import com.gitee.swsk33.chainofresponsibility.handler.impl.FrightenedValidator;
import com.gitee.swsk33.chainofresponsibility.handler.impl.TakingValidator;
import com.gitee.swsk33.chainofresponsibility.model.StudentBehavior;

/**
 * 用于初始化和管理抽烟行为的责任链的上下文
 */
public class SmokeValidatorContext {

	/**
	 * 责任链上的第一个处理者（入口）
	 */
	private static final SmokeValidator entryPoint;

	// 初始化全部校验器对象
	// 校验器顺序：是否弹烟灰 -> 是否刮番茄酱 -> 是否会心虚
	static {
		// 实例化全部校验器
		SmokeValidator flick = new FlickAshesValidator();
		SmokeValidator taking = new TakingValidator();
		SmokeValidator frightened = new FrightenedValidator();
		// 设定顺序
		flick.setNext(taking);
		taking.setNext(frightened);
		// 设定入口校验器
		entryPoint = flick;
	}

	/**
	 * 调用责任链校验学生行为（调用者从此调用）
	 *
	 * @param behavior 学生行为
	 * @return 校验结果
	 */
	public static boolean validateStudentBehavior(StudentBehavior behavior) {
		return entryPoint.validateBehavior(behavior);
	}

}