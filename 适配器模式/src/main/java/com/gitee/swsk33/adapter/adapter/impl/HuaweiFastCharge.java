package com.gitee.swsk33.adapter.adapter.impl;

import com.gitee.swsk33.adapter.adapter.PowerAdapter;
import com.gitee.swsk33.adapter.model.AC220V;

/**
 * 华为40W快充适配器
 */
public class HuaweiFastCharge implements PowerAdapter {

	private AC220V ac220V;

	/**
	 * 构造器依赖注入（模拟华为充电器接入220V交流电）
	 *
	 * @param ac220V 接入的交流电
	 */
	public HuaweiFastCharge(AC220V ac220V) {
		this.ac220V = ac220V;
	}

	@Override
	public int doConvert() {
		// 转换为10V电流
		int output = ac220V.output() / 22;
		return output;
	}

}