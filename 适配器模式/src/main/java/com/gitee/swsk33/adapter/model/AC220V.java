package com.gitee.swsk33.adapter.model;

/**
 * 220V交流电
 */
public class AC220V {

	/**
	 * 接入交流电并获取
	 *
	 * @return 获取到220V交流电
	 */
	public int output() {
		return 220;
	}

}