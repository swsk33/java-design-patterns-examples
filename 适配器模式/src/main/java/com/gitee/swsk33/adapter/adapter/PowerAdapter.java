package com.gitee.swsk33.adapter.adapter;

/**
 * 电源适配器接口
 */
public interface PowerAdapter {

	/**
	 * 对输入电源作转换
	 *
	 * @return 转换后电流
	 */
	int doConvert();

}