package com.gitee.swsk33.adapter;

import com.gitee.swsk33.adapter.adapter.PowerAdapter;
import com.gitee.swsk33.adapter.adapter.impl.AppleCharge;
import com.gitee.swsk33.adapter.adapter.impl.HuaweiFastCharge;
import com.gitee.swsk33.adapter.model.AC220V;

public class Main {

	public static void main(String[] args) {
		PowerAdapter huaweiAdapter = new HuaweiFastCharge(new AC220V());
		System.out.println("华为充电器，得到电压：" + huaweiAdapter.doConvert());
		PowerAdapter appleAdapter = new AppleCharge(new AC220V());
		System.out.println("苹果充电器，得到电压：" + appleAdapter.doConvert());
	}

}