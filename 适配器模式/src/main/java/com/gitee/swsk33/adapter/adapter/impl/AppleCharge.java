package com.gitee.swsk33.adapter.adapter.impl;

import com.gitee.swsk33.adapter.adapter.PowerAdapter;
import com.gitee.swsk33.adapter.model.AC220V;

/**
 * 苹果5V手机充电器
 */
public class AppleCharge implements PowerAdapter {

	private AC220V ac220V;

	/**
	 * 构造器依赖注入（模拟苹果充电器接入220V电源）
	 *
	 * @param ac220V
	 */
	public AppleCharge(AC220V ac220V) {
		this.ac220V = ac220V;
	}

	@Override
	public int doConvert() {
		int output = ac220V.output() / 44;
		return output;
	}

}