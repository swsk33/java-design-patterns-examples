package com.gitee.swsk33.publishsubscribedemo;

import com.gitee.swsk33.publishsubscribedemo.model.*;
import com.gitee.swsk33.publishsubscribedemo.model.prototype.Publisher;
import com.gitee.swsk33.publishsubscribedemo.model.prototype.Subscriber;

/**
 * 主类客户端调用
 */
public class Main {

	public static void main(String[] args) throws Exception {
		// 话题
		final String TOPIC_TEACHER = "teacher";
		final String TOPIC_DEAN = "dean";
		// 事件总线
		Broker broker = new Broker();
		// 消息订阅者
		Subscriber tvStudent = new StudentWatchingTV("小吴", "甄嬛传");
		Subscriber gameStudent = new StudentPlayingGame("小李", "原神");
		// 订阅消息，一个人订阅“老师”话题，一个人订阅“院长”话题
		broker.subscribe(TOPIC_TEACHER, tvStudent);
		broker.subscribe(TOPIC_DEAN, gameStudent);
		// 延迟一小会
		System.out.println("大家正在摸鱼...");
		Thread.sleep(1000);
		// 发布者发布消息
		Publisher gateStudent = new GateKeeper(broker);
		gateStudent.publish(new Event<>(TOPIC_TEACHER, "张老师正在走廊上过来了"));
		gateStudent.publish(new Event<>(TOPIC_DEAN, "刘院长已经到研究室门口看着了"));
	}

}