package com.gitee.swsk33.publishsubscribedemo.model;

import com.gitee.swsk33.publishsubscribedemo.model.prototype.Publisher;

/**
 * 具体发布者：在门口的同学
 */
public class GateKeeper extends Publisher {

	/**
	 * 构造函数
	 *
	 * @param broker 传入事件总线对象
	 */
	public GateKeeper(Broker broker) {
		super(broker);
	}

}