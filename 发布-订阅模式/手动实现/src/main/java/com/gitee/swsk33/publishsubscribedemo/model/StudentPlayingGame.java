package com.gitee.swsk33.publishsubscribedemo.model;

import com.gitee.swsk33.publishsubscribedemo.model.prototype.Subscriber;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 具体订阅者：正在打游戏的学生
 */
@Data
@AllArgsConstructor
public class StudentPlayingGame implements Subscriber {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 游戏名
	 */
	private String gameName;

	@Override
	public void onSubscribe(Event<?> event) {
		// 实现自定义的订阅到消息后的逻辑
		System.out.printf("[%s] 接收到：%s，不要玩%s了！\n", name, event.getData(), gameName);
	}

}