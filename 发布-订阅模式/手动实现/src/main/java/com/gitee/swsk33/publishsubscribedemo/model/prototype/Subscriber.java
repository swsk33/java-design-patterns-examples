package com.gitee.swsk33.publishsubscribedemo.model.prototype;

import com.gitee.swsk33.publishsubscribedemo.model.Event;

/**
 * 抽象订阅者接口
 */
public interface Subscriber {

	/**
	 * 订阅到事件后的逻辑，由子类实现
	 *
	 * @param event 订阅到的事件
	 */
	void onSubscribe(Event<?> event);

}