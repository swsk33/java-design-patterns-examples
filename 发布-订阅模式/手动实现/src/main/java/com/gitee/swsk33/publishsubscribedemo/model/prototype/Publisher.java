package com.gitee.swsk33.publishsubscribedemo.model.prototype;

import com.gitee.swsk33.publishsubscribedemo.model.Broker;
import com.gitee.swsk33.publishsubscribedemo.model.Event;

/**
 * 抽象发布者
 */
public abstract class Publisher {

	/**
	 * 发布到该事件总线
	 */
	private final Broker broker;

	/**
	 * 构造函数
	 *
	 * @param broker 传入事件总线对象
	 */
	public Publisher(Broker broker) {
		this.broker = broker;
	}

	/**
	 * 发布事件
	 *
	 * @param event 要发布的事件对象
	 */
	public void publish(Event<?> event) {
		broker.publish(event);
	}

}