package com.gitee.swsk33.publishsubscribedemo.model;

import com.gitee.swsk33.publishsubscribedemo.model.prototype.Subscriber;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 具体订阅者：正在看电视剧的同学
 */
@Data
@AllArgsConstructor
public class StudentWatchingTV implements Subscriber {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 电视剧名字
	 */
	private String tvName;

	@Override
	public void onSubscribe(Event<?> event) {
		// 实现自定义的订阅到消息后的逻辑
		System.out.printf("[%s] 接收到：%s，关掉%s...\n", name, event.getData(), tvName);
	}

}