package com.gitee.swsk33.publishsubscribereactordemo.model.publisher;

import com.gitee.swsk33.publishsubscribereactordemo.model.Event;
import reactor.core.publisher.FluxSink;

/**
 * 具体发布者：在门口的同学
 */
public class GateKeeper {

	/**
	 * FluxSink是Flux对象异步发布元素时，用于操作元素发布的对象
	 */
	private FluxSink<Event<String>> sink;

	/**
	 * 设定sink对象
	 *
	 * @param sink FluxSink对象
	 */
	public void setupSink(FluxSink<Event<String>> sink) {
		this.sink = sink;
	}

	/**
	 * 发布一个事件
	 *
	 * @param event 发布的事件对象
	 */
	public void publish(Event<String> event) {
		if (sink != null) {
			sink.next(event);
		}
	}

}