package com.gitee.swsk33.publishsubscribereactordemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 事件对象
 *
 * @param <T> 事件内容的具体类型
 */
@Data
@AllArgsConstructor
public class Event<T> {

	/**
	 * 事件主题，用于区分不同的订阅频道
	 */
	private String topic;

	/**
	 * 事件内容
	 */
	private T data;

}