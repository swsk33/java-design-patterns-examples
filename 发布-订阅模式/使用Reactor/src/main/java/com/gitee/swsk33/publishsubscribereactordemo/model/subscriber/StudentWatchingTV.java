package com.gitee.swsk33.publishsubscribereactordemo.model.subscriber;

import com.gitee.swsk33.publishsubscribereactordemo.model.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;

/**
 * 具体订阅者：正在看电视剧的同学
 */
@Data
@AllArgsConstructor
public class StudentWatchingTV extends BaseSubscriber<Event<String>> {

	/**
	 * 名字
	 */
	private String name;

	/**
	 * 电视剧名字
	 */
	private String tvName;

	/**
	 * 订阅开始时操作
	 */
	@Override
	public void hookOnSubscribe(Subscription subscription) {
		System.out.printf("[%s]准备订阅消息！\n", name);
		// 请求订阅一个元素
		request(1);
	}

	/**
	 * 每次订阅到元素的操作
	 *
	 * @param value 订阅到的元素
	 */
	@Override
	public void hookOnNext(Event<String> value) {
		System.out.printf("[%s]接收到：%s，关掉%s...\n", name, value.getData(), tvName);
		// 继续请求订阅下一个元素
		request(1);
	}

}