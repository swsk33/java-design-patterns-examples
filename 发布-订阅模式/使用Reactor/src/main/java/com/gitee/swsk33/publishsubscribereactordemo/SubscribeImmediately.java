package com.gitee.swsk33.publishsubscribereactordemo;

import com.gitee.swsk33.publishsubscribereactordemo.model.Event;
import reactor.core.publisher.Flux;

/**
 * 主类客户端调用<br>
 * 发布后立即订阅的场景
 */
public class SubscribeImmediately {

	public static void main(String[] args) throws Exception {
		// Flux对象，用于传递事件给订阅者
		// “老师”话题发布者
		Flux<Event<String>> teacherFlux = Flux.create(sink -> {
			// create方法编写发布元素的逻辑，sink参数是FluxSink类型对象，用于操纵元素的发布等等
			// next方法发布一个元素
			sink.next(new Event<>("teacher", "张老师正在走廊上"));
			// complete方法结束元素发布，告诉订阅者已完成全部元素发布
			sink.complete();
		});
		// “院长”话题发布者
		Flux<Event<String>> deanFlux = Flux.create(sink -> {
			sink.next(new Event<>("dean", "院长已经在研究室门口看着了"));
			sink.complete();
		});
		// 延迟一会
		System.out.println("大家正在摸鱼...");
		Thread.sleep(1000);
		// 订阅Flux
		teacherFlux.subscribe(event -> {
			// 订阅“老师”话题的学生订阅逻辑
			System.out.printf("[%s] 接收到：%s，关掉%s...\n", "小吴", event.getData(), "甄嬛传");
		});
		deanFlux.subscribe(event -> {
			// 订阅“院长”话题的学生订阅逻辑
			System.out.printf("[%s] 接收到：%s，不要玩%s了！\n", "小李", event.getData(), "原神");
		});
	}

}