package com.gitee.swsk33.strategy.promotion;

import com.gitee.swsk33.strategy.model.Order;

/**
 * 促销活动抽象接口
 */
public interface Promotion {

	/**
	 * 执行促销策略
	 *
	 * @param order 传入订单进行相应折扣
	 */
	void doPromotion(Order order);

}