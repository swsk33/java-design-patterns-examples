package com.gitee.swsk33.strategy;

import com.gitee.swsk33.strategy.model.Order;
import com.gitee.swsk33.strategy.promotion.PromotionContext;
import com.gitee.swsk33.strategy.promotion.PromotionStrategy;

public class Client {

	/**
	 * 客户端进行支付
	 *
	 * @param order    待支付账单
	 * @param strategy 要使用的优惠策略
	 */
	private static void doPayment(Order order, PromotionStrategy strategy) {
		System.out.println("账单：" + order.getName() + "准备支付");
		System.out.println("准备使用优惠");
		PromotionContext.usePromotion(order, strategy);
		System.out.println("支付完成：" + order.getPrice() + "元");
	}

	public static void main(String[] args) {
		// 模拟买东西
		Order order = new Order();
		order.setName("守望时空33购买的辛鹿咖啡豆：曼特宁拼配 深度烘焙1kg，意式极深炭烧 极深烘焙1kg，云南阿拉比卡庄园豆 深度烘焙454g");
		order.setPrice(202);
		// 进行支付，使用满减优惠策略
		doPayment(order, PromotionStrategy.FULLDISCOUNT);
	}

}