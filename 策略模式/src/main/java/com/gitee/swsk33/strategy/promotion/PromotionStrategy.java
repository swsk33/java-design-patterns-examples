package com.gitee.swsk33.strategy.promotion;

/**
 * 优惠策略枚举
 */
public enum PromotionStrategy {
	/**
	 * 不使用优惠
	 */
	NO,
	/**
	 * 满减优惠
	 */
	FULLDISCOUNT,
	/**
	 * 返现优惠
	 */
	CASHBACK
}