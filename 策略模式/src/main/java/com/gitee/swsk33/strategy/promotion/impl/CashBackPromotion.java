package com.gitee.swsk33.strategy.promotion.impl;

import com.gitee.swsk33.strategy.model.Order;
import com.gitee.swsk33.strategy.promotion.Promotion;

/**
 * 返现促销
 */
public class CashBackPromotion implements Promotion {

	@Override
	public void doPromotion(Order order) {
		// 满200返现20
		if (order.getPrice() > 200) {
			// 返现20...
			System.out.println("使用了满200返现20到支付宝账户优惠");
		}
	}

}