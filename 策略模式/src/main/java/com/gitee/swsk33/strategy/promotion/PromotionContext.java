package com.gitee.swsk33.strategy.promotion;

import com.gitee.swsk33.strategy.model.Order;
import com.gitee.swsk33.strategy.promotion.impl.NoPromotion;
import com.gitee.swsk33.strategy.promotion.impl.CashBackPromotion;
import com.gitee.swsk33.strategy.promotion.impl.FullDiscountPromotion;

import java.util.HashMap;
import java.util.Map;

/**
 * 优惠策略选择上下文，用于选择优惠策略
 */
public class PromotionContext {

	// 用一个Map作为容器储存各个优惠策略的类，以枚举为参数取出
	private static Map<PromotionStrategy, Promotion> promotionMap = new HashMap<>();

	// 静态块用于初始化各个优惠策略实例
	static {
		promotionMap.put(PromotionStrategy.NO, new NoPromotion());
		promotionMap.put(PromotionStrategy.FULLDISCOUNT, new FullDiscountPromotion());
		promotionMap.put(PromotionStrategy.CASHBACK, new CashBackPromotion());
	}

	/**
	 * 使用优惠
	 *
	 * @param order    传入要使用优惠的订单
	 * @param strategy 传入优惠策略
	 */
	public static void usePromotion(Order order, PromotionStrategy strategy) {
		Promotion getPromotion = promotionMap.get(strategy);
		getPromotion.doPromotion(order);
	}

}