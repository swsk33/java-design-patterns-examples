package com.gitee.swsk33.strategy.promotion.impl;

import com.gitee.swsk33.strategy.model.Order;
import com.gitee.swsk33.strategy.promotion.Promotion;

/**
 * 不使用优惠
 */
public class NoPromotion implements Promotion {

	@Override
	public void doPromotion(Order order) {
		System.out.println("不使用任何优惠");
	}

}