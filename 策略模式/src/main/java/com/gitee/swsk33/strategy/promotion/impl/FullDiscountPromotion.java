package com.gitee.swsk33.strategy.promotion.impl;

import com.gitee.swsk33.strategy.model.Order;
import com.gitee.swsk33.strategy.promotion.Promotion;

/**
 * 满减促销
 */
public class FullDiscountPromotion implements Promotion {

	@Override
	public void doPromotion(Order order) {
		// 满200减20
		if (order.getPrice() > 200) {
			order.setPrice(order.getPrice() - 20);
			System.out.println("使用了满200减20优惠");
		}
	}

}