package com.gitee.swsk33.strategy.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单类
 */
@Getter
@Setter
public class Order {

	/**
	 * 订单名
	 */
	private String name;

	/**
	 * 订单价额
	 */
	private int price;

}